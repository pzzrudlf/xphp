# xphp 
### Yet another PHP Framework based on MVC

### How to install
```
git clone pzzrudlf@bitbucket.org:pzzrudlf/xphp.git   
cd xphp   
composer update   
/path/to/php -S localhost:81 -t ./   

```

# Notice:
    This framework is used for learning MVC framework, do not use it in production.
enjoy:)
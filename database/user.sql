/*
Navicat MariaDB Data Transfer

Source Server         : 本地连接
Source Server Version : 100206
Source Host           : localhost:3306
Source Database       : xphp

Target Server Type    : MariaDB
Target Server Version : 100206
File Encoding         : 65001

Date: 2017-10-09 07:33:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 10,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'test1', 'pG7TRyTIXlEbcenpi34TzmMYS2zDsMTF', '$2y$13$h6lWl3oaBnWt5TwAqi50NuZuYWMzGQxK9Xg8UW69OuVkdJW3r5FoW', null, 'test1@test.com', '10', '1462597929', '1504511499');
INSERT INTO `user` VALUES ('2', 'test2', 'xqGDBMlylihvNddSQgDkjAdpJwV4d02C', '$2y$13$bJC0vECI9EPLq/kia9CAmOT060fxoT/HopseOnY.C9siZJDOoQguK', null, 'test2@test.com', '10', '1475850924', '1475850924');
INSERT INTO `user` VALUES ('3', 'test3', 'zpuLBdoapK7UA-6EfUGKLPJ5N22vf96c', '$2y$13$h6lWl3oaBnWt5TwAqi50NuZuYWMzGQxK9Xg8UW69OuVkdJW3r5FoW', null, 'test3@test.com', '10', '1504503946', '1504503946');

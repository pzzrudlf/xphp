<?php
/**
 * Created by PhpStorm.
 * User: pzzrudlf
 * Date: 2017/10/8 0008
 * Time: 16:03
 */
namespace app\controller;

use app\models\user;
use \core\xphp;

class IndexController extends xphp
{

    public function index()
    {
        $model = new user();
        $user = $model->lists();
        $this->assign('user', $user);
        $this->display('index/index.html');
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: pzzrudlf
 * Date: 2017/10/8 0008
 * Time: 19:01
 */
namespace app\models;

use core\lib\model;

class user extends model
{
    public $table = 'user';

    public function lists()
    {
        $res = $this->select($this->table, '*');
        return $res;
    }

}
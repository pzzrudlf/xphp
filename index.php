<?php
/**
 * 入口文件
 * 1.定义常量
 * 2.加载函数类库
 * 3.启动框架
 */
define('XPHP', realpath('./'));
define('CORE', XPHP.'/core');
define('APP', XPHP.'/app');
define('MODULE', 'app');
define('DEBUG', true);

include "vendor/autoload.php";

if (DEBUG) {
    $whoops = new \Whoops\Run;
    $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
    $whoops->register();
    ini_set('display_error', 'On');
} else {
    ini_set('display_error', 'Off');
}

//dump($_SERVER);die;

include CORE.'/common/function.php';

include CORE.'/xphp.php';

spl_autoload_register('\core\xphp::load');

\core\xphp::run();
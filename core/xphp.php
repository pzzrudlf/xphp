<?php
/**
 * Created by PhpStorm.
 * User: pzzrudlf
 * Date: 2017/10/8 0008
 * Time: 15:21
 */
namespace core;

use core\lib\log;

class xphp
{
    public static $classMap = [];
    public $assign = [];

    public static function run()
    {
        log::init();
        $route = new \core\lib\route();
        $controller = $route->controller;
        $action = $route->action;
        $controllerFile = APP.'/controller/'.$controller.'Controller.php';
        $controllerClass = '\\'.MODULE.'\controller\\'.$controller.'Controller';
        if (is_file($controllerFile)) {
            include $controllerFile;
            $ctrl = new $controllerClass();
            $ctrl->$action();
        } else {
            throw new \Exception('Not found the controller'.$controllerClass);
        }
    }

    /**
     * 自动加载类库
     */
    public static function load($class)
    {
        if (isset($classMap[$class])) {
            return true;
        } else {
            $class = str_replace('\\', '/', $class);
            $file = XPHP.'/'.$class.'.php';
            if (is_file($file)) {
                include $file;
                self::$classMap[$class] = $class;
            } else {
                return false;
            }
        }
    }

    public function assign($name, $value)
    {
        $this->assign[$name] = $value;
    }

    public function display($file)
    {
        $tmpPath = APP.'/views';
        $tmpFile = $tmpPath.'/'.$file;
        if (is_file($tmpFile)) {
//            extract($this->assign);
//            include APP.'/views/'.$file;die;

            $loader = new \Twig_Loader_Filesystem($tmpPath);
            $twig = new \Twig_Environment($loader, array(
                'cache' => APP.'/runtime/twig',
                'debug' => DEBUG
            ));
            $template = $twig->loadTemplate($file);
            $template->display($this->assign);
        }
    }
}
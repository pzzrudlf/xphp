<?php
/**
 * Created by PhpStorm.
 * User: pzzrudlf
 * Date: 2017/10/8 0008
 * Time: 16:17
 */
namespace core\lib;

use core\lib\config;
use Medoo\Medoo;;

class model extends Medoo
{
    public function __construct()
    {
        $database = config::all('database');
        parent::__construct($database);
    }
}
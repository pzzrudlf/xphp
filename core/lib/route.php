<?php
/**
 * Created by PhpStorm.
 * User: pzzrudlf
 * Date: 2017/10/8 0008
 * Time: 15:31
 */
namespace core\lib;

use core\lib\config;
use core\lib\log;

class route
{
    /**
     * route constructor.
     * 1.隐藏index.php
     * 2.获取URL参数部分
     * 3.返回对应控制器和方法
     */
    public function __construct()
    {
        if (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] != '/') {
            $path = $_SERVER['REQUEST_URI'];
            $pathArray = explode('/', trim($path, '/'));
            if (isset($pathArray[0])) {
                $this->controller = ucfirst($pathArray[0]);
            }
            unset($pathArray[0]);
            if (isset($pathArray[1])) {
                $this->action = $pathArray[1];
                unset($pathArray[1]);
            } else {
                $this->action = config::get('ACTION', 'route');
            }

            $count = count($pathArray) + 2;
            $i = 2;
            while($i < $count) {
                if (isset($pathArray[$i+1])) {
                    $_GET[$pathArray[$i]] = $pathArray[$i+1];
                }
                $i+=2;
            }
        } else {
            $this->controller = ucfirst(config::get('CONTROLLER', 'route'));
            $this->action = config::get('ACTION', 'route');
        }
        log::log("controller is {$this->controller} and action is {$this->action}", 'server');
    }
}
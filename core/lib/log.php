<?php
/**
 * Created by PhpStorm.
 * User: pzzrudlf
 * Date: 2017/10/8 0008
 * Time: 17:20
 */
namespace core\lib;

use core\lib\config;

class log
{
    public static $class = null;

    /**
     * 1.确定日志存储方式
     *
     * 2.写日志
     */
    public static function init()
    {
        $drive = config::get('DRIVE', 'log');
        $class = '\core\lib\drive\log\\'.$drive;
        self::$class = new $class;
    }

    public static function log($message, $file='log')
    {
        self::$class->log($message, $file);
    }
}
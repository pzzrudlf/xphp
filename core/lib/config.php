<?php
/**
 * Created by PhpStorm.
 * User: pzzrudlf
 * Date: 2017/10/8 0008
 * Time: 16:46
 */
namespace core\lib;

class config
{
    public static $conf = [];

    /**
    * 1.判断配置文件是否存在
    * 2.判断配置是否存在
    * 3.缓存配置
    */
    public static function get($name, $file)
    {
        if (isset(self::$conf[$file])) {
            return self::$conf[$file][$name];
        } else {
            $path = XPHP.'/core/config/'.$file.'.php';
            if (is_file($path)) {
                $conf = include $path;
                if (isset($conf[$name])) {
                    self::$conf[$file] = $conf;
                    return $conf[$name];
                } else {
                    throw new \Exception('There is no such config item.');
                }
            } else {
                throw new \Exception('Not found config File '. $file);
            }
        }

    }

    public static function all($file)
    {
        if (isset(self::$conf[$file])) {
            return self::$conf[$file];
        } else {
            $path = XPHP.'/core/config/'.$file.'.php';
            if (is_file($path)) {
                $conf = include $path;
                self::$conf[$file] = $conf;
                return $conf;
            } else {
                throw new \Exception('Not found config File '. $file);
            }
        }
    }
}
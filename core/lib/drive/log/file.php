<?php
/**
 * Created by PhpStorm.
 * User: pzzrudlf
 * Date: 2017/10/8 0008
 * Time: 17:21
 */
namespace core\lib\drive\log;

use core\lib\config;

class file
{
    public $path;

    public function __construct()
    {
        $config = config::get('OPTION', 'log');
        $this->path = $config['PATH'];
    }

    public function log($message, $file = 'log')
    {
        $filePath = $this->path.'/'.date('YmdH');
        if (!is_dir($filePath)) {
            mkdir($filePath, '0777', true);
        }
        return file_put_contents($filePath.'/'.$file.'.php', date('Y-m-d H:i:s').'  '.json_encode($message).PHP_EOL, FILE_APPEND);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: pzzrudlf
 * Date: 2017/10/8 0008
 * Time: 17:22
 */
return [
    'DRIVE' => 'file',
    'OPTION' => [
        'PATH' => APP.'/runtime/cache/log',
    ],
];